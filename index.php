<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        .container {
            max-width: 600px;
            margin: 50px auto;
        }

        form {
            display: flex;
            margin-bottom: 20px;
        }

        input {
            flex: 1;
            padding: 10px;
        }

        button {
            padding: 10px;
        }

        .task {
            margin-bottom: 10px;
            padding: 10px;
            border: 1px solid #ccc;
        }

        .task.done {
            background-color: #9fdf9f; /* Vert pour les tâches terminées */
        }

        .task.pending {
            background-color: #f99; /* Rouge pour les tâches en attente */
        }

        .task button {
            margin-left: 10px;
        }
    </style>
    <title>ToDo List</title>
</head>
<body>
    <div class="container">
        <h1>ToDo List</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            <input type="text" name="task" placeholder="Nouvelle tâche" required>
            <button type="submit">Ajouter</button>
        </form>

        <?php
        // Connexion à la base de données
        $servername = "localhost";
        $username = "votre_nom_utilisateur";
        $password = "votre_mot_de_passe";
        $dbname = "todolist";

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("La connexion à la base de données a échoué : " . $conn->connect_error);
        }

        // Traitement du formulaire d'ajout de tâche
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $task = $_POST['task'];
            $sql = "INSERT INTO todo (title) VALUES ('$task')";
            $conn->query($sql);
        }

        // Traitement de l'action "Terminer" ou "Annuler"
        if (isset($_POST['action'])) {
            $taskId = $_POST['taskId'];

            if ($_POST['action'] == 'toggle') {
                // Basculer l'état de la tâche (terminée ou non terminée)
                $sql = "UPDATE todo SET done = NOT done WHERE id = $taskId";
                $conn->query($sql);
            } elseif ($_POST['action'] == 'delete') {
                // Supprimer la tâche
                $sql = "DELETE FROM todo WHERE id = $taskId";
                $conn->query($sql);
            }
        }

        // Récupérer les tâches depuis la base de données
        $sql = "SELECT * FROM todo ORDER BY created_at DESC";
        $result = $conn->query($sql);

        // Afficher les tâches
        while ($row = $result->fetch_assoc()) {
            $taskId = $row['id'];
            $taskTitle = $row['title'];
            $taskStatus = $row['done'];
            
            echo "<form action='{$_SERVER['PHP_SELF']}' method='post'>";
            echo "<div class='task " . ($taskStatus ? 'done' : 'pending') . "'>";
            echo "<span>$taskTitle</span>";
            echo "<input type='hidden' name='taskId' value='$taskId'>";
            echo "<button type='submit' name='action' value='toggle'>" . ($taskStatus ? 'Annuler' : 'Terminer') . "</button>";
            echo "<button type='submit' name='action' value='delete'>...</button>";
            echo "</div>";
            echo "</form>";
        }

        // Fermer la connexion
        $conn->close();
        ?>
    </div>
</body>
</html>